﻿namespace Monitoring_Service.AppConfigManager
{
    public interface IConfigManager
    {
        string UserName { get; }
        string Password { get; }
        string AuthenticationType { get; }
        string SpeechMinerURL { get; }
        string LoginURL { get; }
        string MonitoringURL { get; }
        string LogoutURL { get; }
        string EventSourceName { get; }
        string EventLogName { get; }
        int MonitoringTime { get; }
        int LowDiskSpaceThreshold { get; }
        string RemoteMachineName { get; }
        string RemoteUserName { get; }
        string RemotePassword { get; }
        string ServiceName { get; }
        string SpeechMinerDBConnection { get; }
        void SetConfigValue(string key, string value);
    }
}
