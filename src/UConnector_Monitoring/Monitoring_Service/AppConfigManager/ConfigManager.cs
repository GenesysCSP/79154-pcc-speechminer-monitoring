﻿using System;
using System.Configuration;

namespace Monitoring_Service.AppConfigManager
{
    public class ConfigManager : IConfigManager
    {
        public string UserName => ConfigurationManager.AppSettings["username"];

        public string Password => ConfigurationManager.AppSettings["password"];

        public string SpeechMinerURL => ConfigurationManager.AppSettings["speechMinerURL"];

        public string LoginURL => ConfigurationManager.AppSettings["loginURL"];

        public string MonitoringURL => ConfigurationManager.AppSettings["monitoringURL"];

        public string LogoutURL => ConfigurationManager.AppSettings["logoutURL"];

        public string AuthenticationType => ConfigurationManager.AppSettings["authenticationType"];

        public string EventSourceName => ConfigurationManager.AppSettings["eventSourceName"];

        public string EventLogName => ConfigurationManager.AppSettings["eventLogName"];

        public int MonitoringTime => Convert.ToInt32(ConfigurationManager.AppSettings["monitoringTime"]);

        public int LowDiskSpaceThreshold => Convert.ToInt32(ConfigurationManager.AppSettings["lowDiskSpaceThreshold"]);

        public string RemoteMachineName => ConfigurationManager.AppSettings["remoteMachineName"];

        public string RemoteUserName => ConfigurationManager.AppSettings["remoteUserName"];

        public string RemotePassword => ConfigurationManager.AppSettings["remotePassword"];

        public string ServiceName => ConfigurationManager.AppSettings["serviceName"];

        public string SpeechMinerDBConnection => ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString;

        public void SetConfigValue(string key, string value)
        {
            ConfigurationManager.AppSettings[key] = value;
        }
    }
}
