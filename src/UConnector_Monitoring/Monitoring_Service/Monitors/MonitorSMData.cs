﻿using log4net;
using Monitoring_Service.AppConfigManager;
using Monitoring_Service.Common;
using Monitoring_Service.LogManager;
using Monitoring_Service.Monitors.Attributes;
using Monitoring_Service.Monitors.Entity;
using Monitoring_Service.Monitors.Interface;
using System;
using System.Threading.Tasks;

namespace Monitoring_Service.Monitors
{
    public class MonitorSMData : IMonitorController
    {
        private readonly IHttpWebManager httpWebManager;
        private readonly IConfigManager configManager;
        private readonly ILogger eventLogger;
        private readonly ISpeechMinerMonitorAPI speechMinerMonitorAPI;
        private static readonly ILog log = Log4NetLogger.GetLog4NetLogger;

        public MonitorSMData(IConfigManager configManager,
            ILogger logger)
        {
            this.configManager = configManager;
            eventLogger = logger;
            httpWebManager = new HttpWebManager(logger);
            speechMinerMonitorAPI = new SpeechMinerMonitorAPI(httpWebManager, this.configManager,
                new SpeechMinerLogin(httpWebManager, this.configManager));
        }

        public async Task BeginMonitoring()
        {
            try
            {
                log.Info("SpeechMiner data monitoring begins.");
                var monitorData = await Task.FromResult(speechMinerMonitorAPI.GetSpeechMinerMonitorData());
                var speechMinerTrigger = new SpeechMinerTriggers(monitorData);
                foreach(var triggerProp in speechMinerTrigger.GetType().GetProperties())
                {
                    var eventLogInfo = (EventLogInfoAttribute)Attribute.GetCustomAttribute(triggerProp, typeof(EventLogInfoAttribute));
                    var message = triggerProp.GetValue(speechMinerTrigger).ToString();
                    eventLogger.WriteMessage(message, eventLogInfo.EventType, eventLogInfo.EventId);
                }
                log.Info("SpeechMiner data monitoring ends.");
            }
            catch (Exception ex)
            {
                log.Error($"Problem in SpeechMiner data monitoring. More details: {ex.Message}");
                eventLogger.WriteMessage(ex.Message, Constants.ERROR, Constants.EVENT_ID_ERROR);
            }
        }
    }
}
