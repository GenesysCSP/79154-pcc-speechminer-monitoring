﻿using System;

namespace Monitoring_Service.Monitors.Attributes
{
    public class EventLogInfoAttribute : Attribute
    {
        public int EventId { get; private set; }
        public string EventType { get; private set; }

        public EventLogInfoAttribute(int eventId, string eventType)
        {
            EventId = eventId;
            EventType = eventType;
        }
    }
}
