﻿using Monitoring_Service.AppConfigManager;
using Monitoring_Service.Common;
using Monitoring_Service.Monitors.Attributes;
using System.Linq;
using System.Text;

namespace Monitoring_Service.Monitors.Entity
{
    public class SpeechMinerTriggers
    {
        private readonly MonitorData monitorData;
        private readonly IConfigManager configManager = new ConfigManager();
        public SpeechMinerTriggers(MonitorData monitorData)
        {
            this.monitorData = monitorData;
        }

        [EventLogInfo(Constants.EVENT_ID_INVALID_PACKAGE_FOLDERS, Constants.INFORMATION)]
        public string InvalidPackageFolders
        {
            get
            {
                var stringBuilder = new StringBuilder();
                foreach (var site in monitorData.Sites.Items)
                {
                    if (site
                        .Folders
                        .Where(x => x.Type == Constants.PACKAGES).Count() > 0
                        )
                        stringBuilder.Append(
                            site.Folders
                            .Where(x => x.Type == Constants.PACKAGES)
                            .Select(x => x.Folder)
                            .FirstOrDefault());
                }
                if (stringBuilder.Length > 0)
                    return string.Format(Constants.MSG_INVALIDPACKAGEFOLDERS, stringBuilder.ToString());
                return string.Format(Constants.MSG_INVALIDPACKAGEFOLDERS, 0);
            }
        }

        [EventLogInfo(Constants.EVENT_ID_LOW_SPACE_FOLDERS, Constants.INFORMATION)]
        public string LowSpaceFolders
        {
            get
            {
                var stringBuilder = new StringBuilder();
                foreach (var site in monitorData.Sites.Items)
                {
                    var folders = site
                        .Folders
                        .Where(x => x.AvailableSpaceMB / Constants.MEMORY_IN_MB < configManager.LowDiskSpaceThreshold);
                    if (folders.Count() > 0)
                        stringBuilder
                            .Append(
                            string.Join("\n", folders.Select(x => $"{x.Folder} : {x.AvailableSpaceMB}")
                            ));
                }
                if (stringBuilder.Length > 0)
                    return string.Format(Constants.MSG_LOWSPACEFOLDERS, stringBuilder.ToString());
                return string.Format(Constants.MSG_LOWSPACEFOLDERS, 0);
            }
        }

        [EventLogInfo(Constants.EVENT_ID_MISSING_PACKAGE, Constants.INFORMATION)]
        public string MissingPackages
        {
            get
            {
                var stringBuilder = new StringBuilder(); 
                foreach (var site in monitorData.Sites.Items)
                {
                    foreach (var computer in site.Computers)
                    {
                        var details = computer.Tasks
                                       .Select(det => det.Details)
                                       .Select(x => x.FindAll(y => !y.Value.Equals("0")));

                        foreach (var detail in details)
                        {
                            if (detail[0].Value.Length > 0)
                                stringBuilder.AppendLine($"{computer.Computer} - Value: {detail[0].Value}");
                        }
                    }
                }
                if (stringBuilder.Length > 0)
                    return string.Format(Constants.MSG_MISSINGPACKAGES, stringBuilder.ToString());
                return string.Format(Constants.MSG_MISSINGPACKAGES, 0);
            }
        }

        [EventLogInfo(Constants.EVENT_ID_WAITING_FOR_CATEGORIZATION, Constants.INFORMATION)]
        public string WaitingForCategorization
        {
            get
            {
                return string.Format(Constants.MSG_WAITINGFORCATEGORIZATION, monitorData.WaitingForCategorization);
            }
        }

        [EventLogInfo(Constants.EVENT_ID_WAITING_FOR_RECOGNITION, Constants.INFORMATION)]
        public string WaitingForRecognition
        {
            get
            {
                return string.Format(Constants.MSG_WAITINGFORRECOGNITION, monitorData.NotProcessedInteractions);
            }
        }

        [EventLogInfo(Constants.EVENT_ID_TOTAL_CATEGORIZED_24H, Constants.INFORMATION)]
        public string TotalCategorized24H
        {
            get
            {
                return string.Format(Constants.MSG_TOTALCATEGORIZED24H, monitorData.TotalCategorized24H);
            }
        }

        [EventLogInfo(Constants.EVENT_ID_TOTAL_RECOGNIZED_24H, Constants.INFORMATION)]
        public string TotalRecognized24H
        {
            get
            {
                return string.Format(Constants.MSG_TOTALRECOGNIZED24H, monitorData.TotalProcessed24H);
            }
        }
    }
}
