﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Monitoring_Service.Monitors.Entity
{
    
    public class Item
    {
        [JsonProperty("queue")]
        public string Queue;

        [JsonProperty("numberOfInteractions")]
        public int NumberOfInteractions;

        [JsonProperty("site")]
        public string Site;

        [JsonProperty("status")]
        public string Status;

        [JsonProperty("folders")]
        public List<PackageFolder> Folders;

        [JsonProperty("computers")]
        public List<SpeechMinerComputer> Computers;
    }

    public class Queues
    {
        [JsonProperty("items")]
        public List<Item> Items;
    }

    public class PackageFolder
    {
        [JsonProperty("folder")]
        public string Folder;

        [JsonProperty("type")]
        public string Type;

        [JsonProperty("status")]
        public string Status;

        [JsonProperty("availableSpaceMB")]
        public int AvailableSpaceMB;

        [JsonProperty("isWritable")]
        public bool IsWritable;
    }

    public class Detail
    {
        [JsonProperty("name")]
        public string Name;

        [JsonProperty("value")]
        public string Value;

        [JsonProperty("status")]
        public string Status;
    }

    public class ComputerTask
    {
        [JsonProperty("task")]
        public string Task;

        [JsonProperty("status")]
        public string Status;

        [JsonProperty("details")]
        public List<Detail> Details;
    }

    public class SpeechMinerComputer
    {
        [JsonProperty("computer")]
        public string Computer;

        [JsonProperty("status")]
        public string Status;

        [JsonProperty("reruns")]
        public int Reruns;

        [JsonProperty("lifesign")]
        public DateTime Lifesign;

        [JsonProperty("lastRerun")]
        public DateTime LastRerun;

        [JsonProperty("activeSessions")]
        public int ActiveSessions;

        [JsonProperty("cpuPercent")]
        public double CpuPercent;

        [JsonProperty("memoryAvailable")]
        public int MemoryAvailable;

        [JsonProperty("tasks")]
        public List<ComputerTask> Tasks;
    }

    public class Sites
    {
        [JsonProperty("items")]
        public List<Item> Items;
    }

    public class MonitorData
    {
        [JsonProperty("allInteractions")]
        public int AllInteractions;

        [JsonProperty("notProcessedInteractions")]
        public int NotProcessedInteractions;

        [JsonProperty("notProcessedAudioDurationInSec")]
        public int NotProcessedAudioDurationInSec;

        [JsonProperty("latestInteractionTime")]
        public DateTime LatestInteractionTime;

        [JsonProperty("lastIndexedInteractionTime")]
        public DateTime LastIndexedInteractionTime;

        [JsonProperty("logErrors")]
        public int LogErrors;

        [JsonProperty("inProcess")]
        public int InProcess;

        [JsonProperty("waitingForCategorization")]
        public int WaitingForCategorization;

        [JsonProperty("waitingForIndexing")]
        public int WaitingForIndexing;

        [JsonProperty("logWarnings")]
        public int LogWarnings;

        [JsonProperty("totalProcessed24H")]
        public int TotalProcessed24H;

        [JsonProperty("totalCategorized24H")]
        public int TotalCategorized24H;

        [JsonProperty("totalIndexed24H")]
        public int TotalIndexed24H;

        [JsonProperty("totalEvaluationSessions24H")]
        public int TotalEvaluationSessions24H;

        [JsonProperty("queues")]
        public Queues Queues;

        [JsonProperty("sites")]
        public Sites Sites;

        [JsonProperty("href")]
        public string Href;

        [JsonProperty("statusCode")]
        public int StatusCode;
    }




}
