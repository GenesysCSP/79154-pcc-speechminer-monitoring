﻿using Dapper;
using log4net;
using Monitoring_Service.AppConfigManager;
using Monitoring_Service.Common;
using Monitoring_Service.LogManager;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Monitoring_Service.Monitors
{
    public class MonitorSLAWarning : IMonitorController
    {
        private readonly IConfigManager configManager;
        private readonly ILogger eventLogger;
        private static readonly ILog log = Log4NetLogger.GetLog4NetLogger;
        private IDbConnection dbConnection;

        public MonitorSLAWarning(IConfigManager configManager, ILogger eventLogger)
        {
            this.configManager = configManager;
            this.eventLogger = eventLogger;
        }

        public async Task BeginMonitoring()
        {
            try
            { 
                var slaWarningCount = await Task<int>.Factory.StartNew(() => GetSLAWarnings());
                eventLogger.WriteMessage($"{Constants.MSG_SLAWARNINGS} {slaWarningCount}", Constants.INFORMATION, Constants.EVENT_ID_SLA_WARNINGS);

            }
            catch (Exception ex)
            {
                log.Error($"Problem in Monitor SLA Warning. More details: {ex.Message}");
                eventLogger.WriteMessage(ex.Message, Constants.ERROR, Constants.EVENT_ID_ERROR);
            }
        }

        private int GetSLAWarnings()
        {
            int slaWarningCount = 0;
            log.Info($"Connecting to SpeechMiner Database.");
            using (dbConnection = new SqlConnection(configManager.SpeechMinerDBConnection))
            {
                log.Info($"Executing the SLA Warning sql query.");
                var reader = dbConnection.ExecuteReader("SELECT * FROM webServiceParams");
                var dt = new DataTable();
                dt.Load(reader);
                if (dt.Rows.Count > 0 && dt.Rows[0]["SlaType"] != null)
                {

                }
                log.Info($"SQL Query executed successfully.");
            }
            return slaWarningCount;
        }
    }
}
