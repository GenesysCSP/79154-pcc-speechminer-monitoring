﻿using log4net;
using Monitoring_Service.AppConfigManager;
using Monitoring_Service.Common;
using Monitoring_Service.LogManager;
using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace Monitoring_Service.Monitors
{
    public class MonitorUPlatformService : IMonitorController
    {
        private readonly IConfigManager configManager;
        private readonly ILogger eventLogger;
        private static readonly ILog log = Log4NetLogger.GetLog4NetLogger;

        public MonitorUPlatformService(IConfigManager configManager, ILogger eventLogger)
        {
            this.configManager = configManager;
            this.eventLogger = eventLogger;
        }

        public async Task BeginMonitoring()
        {
            try
            {
                log.Info($"Checking the status of {configManager.ServiceName} service.");
                ServiceController sc = new ServiceController(configManager.ServiceName);
                log.Info($"Current the status of {configManager.ServiceName} service: {Constants.MSG_UPLATFORM} {sc.Status}");
                await Task.Factory.StartNew(
                    () => eventLogger.WriteMessage($"{Constants.MSG_UPLATFORM} {sc.Status}",
                                                      EventLogEntryType.Information.ToString(),
                                                      Constants.EVENT_ID_UPLATFORM));
            }
            catch (Exception ex)
            {
                log.Error($"Problem in Monitor UPlatform Service. More details: {ex.Message}");
                eventLogger.WriteMessage(ex.Message, Constants.ERROR, Constants.EVENT_ID_ERROR);
            }
            
        }
    }
}
