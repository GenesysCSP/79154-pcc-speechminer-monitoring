﻿using System.Threading.Tasks;

namespace Monitoring_Service.Monitors
{
    public interface IMonitorController
    {
        Task BeginMonitoring();
    }
}
