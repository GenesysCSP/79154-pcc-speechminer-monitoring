﻿using System;
namespace Monitoring_Service
{
    class Program
    {
        static void Main(string[] args)
        {
            IStartup startup = new Startup();
            startup.StartAsync().GetAwaiter().GetResult();
            Console.ReadLine();
        }
    }
}
