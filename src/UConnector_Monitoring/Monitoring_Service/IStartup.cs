﻿using System.Threading.Tasks;

namespace Monitoring_Service
{
    interface IStartup
    {
        Task StartAsync();
        void Stop();
    }
}
