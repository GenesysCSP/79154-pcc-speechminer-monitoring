﻿using System.Diagnostics;

namespace Monitoring_Service.Common
{
    public class Constants
    {
        //Web Method
        public const string GET_METHOD = "GET";
        public const string POST_METHOD = "POST";

        //Logger Manager
        public const string LOGGER_NAME = "Monitoring Data";

        //Event Log
        public const string ERROR = "Error";
        public const string WARNING = "Warning";
        public const string INFORMATION = "Information";

        //Event ID
        public const int EVENT_ID_ERROR = 5000;
        public const int EVENT_ID_INVALID_PACKAGE_FOLDERS = 5001;
        public const int EVENT_ID_LOW_SPACE_FOLDERS = 5002;
        public const int EVENT_ID_MISSING_PACKAGE = 5003;
        public const int EVENT_ID_WAITING_FOR_CATEGORIZATION = 5010;
        public const int EVENT_ID_WAITING_FOR_RECOGNITION = 5011;
        public const int EVENT_ID_TOTAL_RECOGNIZED_24H = 5012;
        public const int EVENT_ID_TOTAL_CATEGORIZED_24H = 5013;
        public const int EVENT_ID_SLA_WARNINGS = 5014;
        public const int EVENT_ID_UPLATFORM = 5017;

        //Trigger Keys
        public const string PACKAGES = "Packages";
        public const string RECOGNITION_MANAGER = "Recognition Manager";

        //Memory 
        public const int MEMORY_IN_MB = 1024;

        //Common
        public const string MESSAGE_OK = "OK";
        public const string SERVICE_NOT_FOUND = "Service not found";

        //Event Message
        public const string MSG_INVALIDPACKAGEFOLDERS = "A list of invalid recognition package folders: {0}";
        public const string MSG_LOWSPACEFOLDERS = "The number of low space disk folders: {0}";
        public const string MSG_MISSINGPACKAGES = "The number of missing packages per machine: {0}";
        public const string MSG_WAITINGFORCATEGORIZATION = "The number of interactions that have not been categorized yet: {0}";
        public const string MSG_WAITINGFORRECOGNITION = "The number of interactions that have not been Recognized yet: {0}";
        public const string MSG_TOTALCATEGORIZED24H = "The number of interactions that have been categorized in the last 24 hours: {0}";
        public const string MSG_TOTALRECOGNIZED24H = "The number of interactions that have been recognized in the last 24 hours: {0}";
        public const string MSG_SLAWARNINGS = "Number of interactions for which the recognition took longer than the recognition SLA time:";
        public const string MSG_UPLATFORM = "Current status of UPlatform service:";
    }
}
