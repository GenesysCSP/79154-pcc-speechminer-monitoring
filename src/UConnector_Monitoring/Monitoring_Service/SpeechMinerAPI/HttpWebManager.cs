﻿using Monitoring_Service.LogManager;
using Monitoring_Service.Monitors.Interface;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring_Service.Monitors
{
    public class HttpWebManager : IHttpWebManager
    {
        private CookieContainer cookieContainer;
        private readonly ILogger logger;

        public HttpWebManager(ILogger logger)
        {
            this.logger = logger;
        }


        public HttpWebRequest GetHttpWebRequest(string uri, string method)
        {
            if (uri == null)
            {
                throw new ArgumentNullException("URI should not be empty");
            }
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = method;
            return request;
        }

        public HttpWebRequest GetHttpWebRequest(string uri, string method, CookieCollection cookieCollection)
        {
            if (uri == null)
            {
                throw new ArgumentNullException("URI should not be empty");
            }
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = method; 
            if (request.CookieContainer == null)
                request.CookieContainer = new CookieContainer();
            request.CookieContainer.Add(cookieCollection);
            return request;
        }

        public HttpWebRequest GetHttpWebRequest(string uri, string method, string bodyContent)
        {
            if (uri == null)
            {
                throw new ArgumentNullException("URI should not be empty");
            }
            var request = (HttpWebRequest)WebRequest.Create(uri);
            try
            {
                request.Method = method;
                cookieContainer = new CookieContainer();
                request.CookieContainer = cookieContainer;
                byte[] byteArray = Encoding.UTF8.GetBytes(bodyContent);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;
                Stream dataStream = request.GetRequestStream();
                try
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }
                finally
                {
                    dataStream.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Problem with HTTPWebRespone. Message Details:{ex.Message}");
            }

            return request;
        }

        public HttpWebResponse GetHttpWebResponse(HttpWebRequest httpWebRequest)
        {
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException ex)
            {
                throw new Exception($"Web exception occurred. Message Details: {ex.Message}");
            }
            catch (Exception ex)
            {
                throw new Exception($"Problem with HTTPWebRespone. Message Details:{ex.Message}");
            }
            return response;
        }

        public string GetResponseContent(HttpWebResponse response)
        {
            if (response == null)
            {
                throw new ArgumentNullException("response");
            }
            Stream dataStream = null;
            StreamReader reader = null;
            try
            {
                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                reader = new StreamReader(dataStream);
                // Read the content.
                return reader.ReadToEnd();
                // Cleanup the streams and the response.
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (dataStream != null)
                {
                    dataStream.Close();
                }
                response.Close();
            }
        } 
    }
}
