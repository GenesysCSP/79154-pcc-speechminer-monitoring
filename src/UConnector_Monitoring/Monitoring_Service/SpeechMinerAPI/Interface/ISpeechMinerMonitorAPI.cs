﻿using Monitoring_Service.Monitors.Entity;
using System.Threading.Tasks;

namespace Monitoring_Service.Monitors.Interface
{
    public interface ISpeechMinerMonitorAPI
    {
        MonitorData GetSpeechMinerMonitorData();
    }
}
