﻿using System.Net;
using System.Threading.Tasks;

namespace Monitoring_Service.Monitors.Interface
{
    public interface IHttpWebManager
    {
        HttpWebRequest GetHttpWebRequest(string uri, string method);
        HttpWebRequest GetHttpWebRequest(string uri, string method, CookieCollection cookieCollection);
        HttpWebRequest GetHttpWebRequest(string uri, string method, string bodyContent);
        HttpWebResponse GetHttpWebResponse(HttpWebRequest httpWebRequest);
        string GetResponseContent(HttpWebResponse response);
    }
}
