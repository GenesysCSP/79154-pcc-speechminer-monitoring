﻿using System.Net;
using System.Threading.Tasks;

namespace Monitoring_Service.Monitors.Interface
{
    public interface ISpeechMinerLogin
    {
        CookieCollection Cookies { get; }
        void Login();
        void Logout();
    }
}
