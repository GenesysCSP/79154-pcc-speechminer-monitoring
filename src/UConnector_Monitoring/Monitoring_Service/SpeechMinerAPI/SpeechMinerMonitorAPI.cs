﻿using log4net;
using Monitoring_Service.AppConfigManager;
using Monitoring_Service.Common;
using Monitoring_Service.LogManager;
using Monitoring_Service.Monitors.Entity;
using Monitoring_Service.Monitors.Interface;
using Newtonsoft.Json;
using System;

namespace Monitoring_Service.Monitors
{
    public class SpeechMinerMonitorAPI : ISpeechMinerMonitorAPI
    {
        private readonly IHttpWebManager httpWebManager;
        private readonly ISpeechMinerLogin speechMinerLogin;
        private readonly IConfigManager configManager;
        private string monitorURL = string.Empty;
        private static readonly ILog log = Log4NetLogger.GetLog4NetLogger;
        public SpeechMinerMonitorAPI(IHttpWebManager httpWebManager,
            IConfigManager configManager,
            ISpeechMinerLogin speechMinerLogin)
        {
            this.httpWebManager = httpWebManager;
            this.speechMinerLogin = speechMinerLogin;
            this.configManager = configManager;
        }

        public MonitorData GetSpeechMinerMonitorData()
        {
            MonitorData monitorData;
            try
            {
                monitorData = new MonitorData();
                log.Info("Login into the SpeechMiner application.");
                speechMinerLogin.Login();
                log.Info("Successfully login into SpeechMiner application.");

                monitorURL = $"{configManager.SpeechMinerURL}{configManager.MonitoringURL}";
                log.Info($"Generate Monitor API request. Request URL: {monitorURL}. Cookies Count: {speechMinerLogin.Cookies.Count}");
                var request = httpWebManager.GetHttpWebRequest(monitorURL, Constants.GET_METHOD, speechMinerLogin.Cookies);

                var response = httpWebManager.GetHttpWebResponse(request);
                log.Info($"Received Monitor API response from speechminer server.");

                var jsonResult = httpWebManager.GetResponseContent(response);
                log.Info($"Converted the response object to string object completed successfully.");
                monitorData = JsonConvert.DeserializeObject<MonitorData>(jsonResult);
                log.Info($"Converted the JSON string object to Monitor data class object completed successfully.");

                speechMinerLogin.Logout();
                log.Info("Successfully logout from SpeechMiner application.");
            }
            catch (Exception ex)
            {
                log.Error("Problem in GetSpeechMinerMonitorData() method.", ex);
                throw ex;
            }
            return monitorData;
        }
    }
}
