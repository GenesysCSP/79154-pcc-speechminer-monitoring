﻿using log4net;
using Monitoring_Service.AppConfigManager;
using Monitoring_Service.Common;
using Monitoring_Service.LogManager;
using Monitoring_Service.Monitors.Interface;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Monitoring_Service.Monitors
{
    public class SpeechMinerLogin : ISpeechMinerLogin
    {
        private readonly IHttpWebManager httpWebManager;
        private readonly IConfigManager configManager;
        private string loginURL = string.Empty;
        private string logoutURL = string.Empty;
        private string bodyContent = string.Empty;
        private CookieContainer _cookieContainer = new CookieContainer();
        private static readonly ILog log = Log4NetLogger.GetLog4NetLogger;

        public SpeechMinerLogin(IHttpWebManager httpWebManager,
            IConfigManager configManager)
        {
            this.httpWebManager = httpWebManager;
            this.configManager = configManager;
        }

        public CookieCollection Cookies
        {
            get
            {
                if (_cookieContainer.Count == 0)
                {
                    Task.Factory.StartNew(() => this.Login());
                }
                return _cookieContainer.GetCookies(new Uri(loginURL));
            }
        }

        public void Login()
        {
            log.Info("Login() method invoked.");
            try
            {
                if (_cookieContainer.Count == 0)
                {
                    loginURL = $"{configManager.SpeechMinerURL}{configManager.LoginURL}";
                    log.Info($"Generate Login URL: {loginURL}");

                    bodyContent = $"user={Uri.EscapeDataString(configManager.UserName)}" +
                        $"&password={Uri.EscapeDataString(configManager.Password)}" +
                        $"&authenticationType={Uri.EscapeDataString(configManager.AuthenticationType)}";

                    log.Info($"Generate request body content: {bodyContent}");
                    log.Info($"Login request initiated.");
                    var request = httpWebManager.GetHttpWebRequest(loginURL, Constants.POST_METHOD, bodyContent);
                    var response = httpWebManager.GetHttpWebResponse(request);
                    log.Info($"Login successully completed. Cookies count: {response.Cookies.Count}");
                    _cookieContainer.Add(response.Cookies);
                    log.Info($"Cookies are added in cookie collection.");
                }
            }
            catch (Exception ex)
            {
                log.Error("Problem in SpeechMiner Login.", ex);
                throw ex;
            }
            log.Info("Login() method ends.");
        }

        public void Logout()
        {
            log.Info("Logout() method invoked.");
            try
            {
                logoutURL = $"{configManager.SpeechMinerURL}{configManager.LogoutURL}";
                log.Info($"Generate Logout URL: {logoutURL}");
                log.Info($"Logout request initiated.");
                var request = httpWebManager.GetHttpWebRequest(loginURL, Constants.GET_METHOD);
                var response = httpWebManager.GetHttpWebResponse(request);
                log.Info($"Logout successully completed.");
            }
            catch (Exception ex)
            {
                log.Error("Problem in SpeechMiner Logout.", ex);
                throw ex;
            }
            log.Info("Logout() method ends.");
        }
    }
}
