﻿using Monitoring_Service.AppConfigManager;
using System;
using System.Diagnostics;

namespace Monitoring_Service.LogManager
{
    public class EventLogger : ILogger
    {
        private readonly EventLog eventLog;
        private readonly IConfigManager configManager = new ConfigManager();
        private static readonly Lazy<EventLogger> eventLogger = new Lazy<EventLogger>(() => new EventLogger());

        private EventLogger()
        {
            if (!EventLog.SourceExists(configManager.EventSourceName))
            {
                EventLog.CreateEventSource(configManager.EventSourceName, configManager.EventLogName);
            }
            eventLog = new EventLog
            {
                Source = configManager.EventSourceName
            };
        }

        public static EventLogger GetLoggerInstance
        {
            get
            {
                return eventLogger.Value;
            }
        }

        public void WriteMessage(string message)
        {
            WriteMessage(message, EventLogEntryType.Information.ToString());
        }

        public void WriteMessage(string message, string eventType)
        {
            WriteMessage(message, eventType, 5000);
        }

        public void WriteMessage(string message, string eventType, int eventId)
        {
            eventLog.WriteEntry(message, (EventLogEntryType)(int)Enum.Parse(typeof(EventLogEntryType), eventType), eventId);
        }
    }
}
