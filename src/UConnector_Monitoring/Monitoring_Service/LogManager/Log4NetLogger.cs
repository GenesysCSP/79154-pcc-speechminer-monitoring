﻿using log4net;
using Monitoring_Service.Common;

namespace Monitoring_Service.LogManager
{
    public class Log4NetLogger
    {
        public static ILog GetLog4NetLogger
        {
            get
            {
                return log4net.LogManager.GetLogger(Constants.LOGGER_NAME);
            }
        }
    }
}
