﻿namespace Monitoring_Service.LogManager
{
    public interface ILogger
    {
        void WriteMessage(string message);
        void WriteMessage(string message, string eventType);
        void WriteMessage(string message, string eventType, int eventId);
    }
}
