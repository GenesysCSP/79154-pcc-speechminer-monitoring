﻿using log4net;
using Monitoring_Service.AppConfigManager;
using Monitoring_Service.LogManager;
using Monitoring_Service.Monitors;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Monitoring_Service
{
    class Startup : IStartup
    {
        private readonly List<IMonitorController> monitorControllers;
        private readonly IConfigManager configManager = new ConfigManager();
        private readonly ILogger eventLogger = EventLogger.GetLoggerInstance;
        private static readonly ILog log = Log4NetLogger.GetLog4NetLogger;
        private List<Task> tasks;
        public Startup()
        {
            log.Info("The Startup class has been initialsized.");
            monitorControllers = new List<IMonitorController>
            {
                new MonitorSMData(configManager, eventLogger),
                new MonitorSLAWarning(configManager, eventLogger),
                new MonitorUPlatformService(configManager, eventLogger)
            };
            log.Info($"The list of monitoring services added. Count = {monitorControllers.Count}");
        }
        public async Task StartAsync()
        {
            TimeSpan timeOut = new TimeSpan(0, 0, configManager.MonitoringTime); 
            while (true)
            {
                log.Info($"The monitoring task started.");
                tasks = new List<Task>();
                foreach (var monitorController in monitorControllers)
                {
                    log.Info($"The {monitorController.GetType().FullName} monitoring task started.");
                    tasks.Add(Task.Run(() => monitorController.BeginMonitoring()));
                }
                await Task.WhenAll(tasks);
                log.Info($"The monitoring task completed.");
                log.Info($"Application set to sleep for {configManager.MonitoringTime} seconds.");
                Thread.Sleep(timeOut);
            }
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }
    }
}
